package conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Tefa
 */
public class Conexion {
    
    // Atributos
    private static String url="jdbc:mysql://localhost:3307/parking";
    private static String user="root";
    private static String password= "1234";
       
    
    // Constructor
    public Conexion(){
        // jdbc:mysql protocolo de conexión a la DB 
        // luego viene el dominio - localhost -> :3306 es el puerto    
    }
    
    // Métodos
//    public Connection iniciarConexion() throws ClassNotFoundException, SQLException {
//        Class.forName("com.mysql.cj.jdbc.Driver");
//        Connection  connection=DriverManager.getConnection(this.url,this.user,this.password);
//        System.out.println("Connection is Successful to the database "+url);
//        return connection;           
//    }
    public static Connection iniciarConexion() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        Connection connection;
        connection = DriverManager.getConnection(url,user,password);
        //System.out.println("Connection is Successful to the database "+url);
        return connection;         
    }
    
        public static ResultSet ejecutarConsulta(String sql) throws ClassNotFoundException, SQLException{        
        Connection conex = iniciarConexion();
        Statement statement = conex.createStatement();
        statement.execute(sql);
        if(sql.toUpperCase().startsWith("SELECT")){
            return statement.getResultSet();
        } else if (sql.toUpperCase().startsWith("UPDATE")){
            return null;
        } else if (sql.toUpperCase().startsWith("DELETE")){
            return null;
        } else if (sql.toUpperCase().startsWith("INSERT")){
            return null;
        }
        statement.close();
        return null;
        }

    public PreparedStatement prepareStatement(String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
        
    
}