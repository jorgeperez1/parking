/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.easyparkingmintic;

import conexion.Conexion;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author JorgeLeoPérez
 */
public class Factura {

    private int id;
    private String placa;
    private String fecha_ingreso;
    private String fecha_salida;
    private String hora_ingreso;
    private String hora_salida;
    private Double valor;
    private int tarifa;

    public Factura(int id, String placa, String fecha_ingreso, String fecha_salida, String hora_ingreso, String hora_salida, Double valor, int tarifa) {
        this.id = id;
        this.placa = placa;
        this.fecha_ingreso = fecha_ingreso;
        this.fecha_salida = fecha_salida;
        this.hora_ingreso = hora_ingreso;
        this.hora_salida = hora_salida;
        this.valor = valor;
        this.tarifa = tarifa;
    }

    Factura() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getFecha_ingreso() {
        return fecha_ingreso;
    }

    public void setFecha_ingreso(String fecha_ingreso) {
        this.fecha_ingreso = fecha_ingreso;
    }

    public String getFecha_salida() {
        return fecha_salida;
    }

    public void setFecha_salida(String fecha_salida) {
        this.fecha_salida = fecha_salida;
    }

    public String getHora_ingreso() {
        return hora_ingreso;
    }

    public void setHora_ingreso(String hora_ingreso) {
        this.hora_ingreso = hora_ingreso;
    }

    public String getHora_salida() {
        return hora_salida;
    }

    public void setHora_salida(String hora_salida) {
        this.hora_salida = hora_salida;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public int getTarifa() {
        return tarifa;
    }

    public void setTarifa(int tarifa) {
        this.tarifa = tarifa;
    }

    //Guardar
    public void guardar() throws ClassNotFoundException, SQLException {
        //CRUD - C
        Conexion con = new Conexion(); //instancia
        String sql = "INSERT INTO facturas(id, placa, fecha_ingreso, fecha_salida, hora_ingreso, hora_salida,valor,tarifa) VALUES(" + getId() + ",'" + getPlaca() + "','" + getFecha_ingreso() + "','" + getFecha_salida() + "','" + getHora_ingreso() + "','" + getHora_salida() + "','" + getValor() + "','" + getTarifa() + "' );";
        ResultSet rs = Conexion.ejecutarConsulta(sql);
        System.out.println("Factura guardada correctamente");

    }
    //Consultar

    public boolean consultar() throws ClassNotFoundException, SQLException {
        //CRUD -R 
        Conexion con = new Conexion();
        String sql = "SELECT id,placa,fecha_ingreso,fecha_salida,hora_ingreso,hora_salida,valor,tarifa FROM facturas WHERE id = '" + this.getId() + "'";
        ResultSet rs = Conexion.ejecutarConsulta(sql);
        while (rs.next()) // 
        {
            this.setId(rs.getInt("id"));
            this.setPlaca(rs.getString("placa"));
            this.setFecha_ingreso(rs.getString("fecha_ingreso"));
            this.setFecha_salida(rs.getString("fecha_salida"));
            this.setHora_ingreso(rs.getString("hora_ingreso"));
            this.setHora_salida(rs.getString("hora_salida"));
            this.setValor(rs.getDouble("valor"));
            this.setTarifa(rs.getInt("tarifa"));

        }

        if (this.getId() != 0) {
            return true;
        } else {
            return false;
        }

    }

    //Actualizar
    public void actualizar() throws ClassNotFoundException, SQLException {
        // CRUD -U
        Conexion con = new Conexion();
        String sql = "UPDATE facturas SET placa = '" + getPlaca() + "',fecha_ingreso = '" + getFecha_ingreso() + "', fecha_salida = '" + getFecha_salida() + "', hora_ingreso = '" + getHora_ingreso() + "', hora_salida = '" + getHora_salida() + "', valor = " + getValor() + " WHERE id =" + getId() + ";";
        ResultSet rs = Conexion.ejecutarConsulta(sql);
        System.out.println("Factura actualizada correctamente");

    }
    //borrar

    public void borrar() throws SQLException, ClassNotFoundException {
        //CRUD -D
        Conexion con = new Conexion();
        String sql = "DELETE FROM facturas WHERE id =" + getId() + ";";
        ResultSet rs = Conexion.ejecutarConsulta(sql);
        System.out.println("Factura borrada correctamente");

    }

    @Override
    public String toString() {
        return "Factura{" + "id=" + id + ", placa=" + placa + ", fecha_ingreso=" + fecha_ingreso + ", fecha_salida=" + fecha_salida + ", hora_ingreso=" + hora_ingreso + ", hora_salida=" + hora_salida + ", valor=" + valor + ", tarifa=" + tarifa + '}';
    }

}
