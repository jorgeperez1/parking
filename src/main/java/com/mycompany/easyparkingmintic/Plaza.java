/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.easyparkingmintic;

import conexion.Conexion;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author JorgeLeoPérez
 */
public class Plaza {

    private int id;
    private String estado;

    public Plaza(int id, String estado) {
        this.id = id;
        this.estado = estado;
    }

    Plaza() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    // Guardar
    public void guardar() throws ClassNotFoundException, SQLException {
        //CRUD - C
        Conexion con = new Conexion(); //instancia
        String sql = "INSERT INTO plazas (id, estado) VALUES(" + getId() + ",'" + getEstado() + "');";
        ResultSet rs = Conexion.ejecutarConsulta(sql);
        System.out.println("Plaza añadida correctamente");
    }
    //Consultar

    public boolean consultar() throws ClassNotFoundException, SQLException {
        //CRUD -R 
        Conexion con = new Conexion();
        String sql = "SELECT id,estado FROM plazas WHERE plazas.estado=1";
        //String sql = "SELECT estado FROM plazas WHERE id = '" + this.getId() + "'";

        ResultSet rs = Conexion.ejecutarConsulta(sql);
        while (rs.next()) // 
        {
            this.setId(rs.getInt("id"));
            this.setEstado(rs.getString("estado"));
            System.out.println("Plaza: " + rs.getString(1) + " Estado : " + rs.getString(2));            

        }

        if (this.getId() != 0) {
            return true;
        } else {
            return false;
        }

    }
        //Consultar

    public boolean consultar2() throws ClassNotFoundException, SQLException {
        //CRUD -R 
        Conexion con = new Conexion();
        String sql = "SELECT id,estado FROM plazas WHERE plazas.estado=1";
        //String sql = "SELECT estado FROM plazas WHERE id = '" + this.getId() + "'";

        ResultSet rs = Conexion.ejecutarConsulta(sql);
        while (rs.next()) // 
        {
            this.setId(rs.getInt("id"));
            this.setEstado(rs.getString("estado"));
            System.out.println("Plaza: " + rs.getString(1) + " Estado : " + rs.getString(2));            

        }

        if (this.getId() != 0) {
            return true;
        } else {
            return false;
        }

    }

    //Actualizar
    public void actualizar() throws ClassNotFoundException, SQLException {
        // CRUD -U
        Conexion con = new Conexion();

        String sql = "UPDATE plazas SET estado = '" + getEstado() + "' WHERE id =" + getId() + ";";

        ResultSet rs = Conexion.ejecutarConsulta(sql);
        System.out.println("Plaza actualizada correctamente");

    }

    //borrar
    public void borrar() throws SQLException, ClassNotFoundException {
        //CRUD -D
        Conexion con = new Conexion();
        String sql = "DELETE FROM plazas WHERE id =" + getId() + ";";
        ResultSet rs = Conexion.ejecutarConsulta(sql);
        System.out.println("Plaza borrada correctamente");

    }

    @Override
    public String toString() {
        return "Plaza{" + "id=" + id + ", estado=" + estado + '}';
    }

}
