/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.easyparkingmintic;

import conexion.Conexion;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import java.sql.SQLException;
import java.time.Duration;
import java.time.LocalTime;
import java.util.Scanner;

/**
 *
 * @author Jorge Pérez
 */
public class Main {

    public long valor;

    public static void main(String[] args) throws SQLException, ClassNotFoundException {

        Scanner s = new Scanner(System.in);
        System.out.println("*********************************");
        System.out.println("Easy Parking" + "\n");
        System.out.println("DIGITE SU ID DE ADMINISTRADOR:  ");
        System.out.println("*********************************");
        int id = s.nextInt();

        Administrador admin = new Administrador();
        admin.setId(id);

        if (admin.consultar()) {
            int opcion = -1; // bandera

//            c.setIdEstudiante(e.getId()); // Ponemos el id_estudiante en la instancia de cuenta c
//            c.consultar(); // Traemos los datos de la tabla cuentas para este estudiante
            while (opcion != 0) {
                System.out.println("*********************************************");
                System.out.println("Seleccione una opción");
                System.out.println("    1. Mirar Plazas Disponibles");
                System.out.println("    2. Registrar Vehículo");
                System.out.println("    3. Generar Factura");
                System.out.println("    4. Mirar Registros");
                System.out.println("    0. Salir");
                System.out.println("*********************************************");

                opcion = s.nextInt();
                if (opcion >= 0 && opcion <= 4) {
                    switch (opcion) {
                        case 1:
                            Plaza e = new Plaza();
                            e.consultar();
                            break;
                        case 2:
                            //instancias
                            Registros c = new Registros();
                            Plaza d = new Plaza();

                            System.out.println("Digite la placa");
                            String placa = s.next(); // pedimos la placa 
                            System.out.println("Digite la plaza");
                            int plaza = s.nextInt(); // pedimos la plaza
                            //System.out.println("Digite la tarifa");
                            //System.out.println("1. para carro 2. para moto");
                            //int tarifa = s.nextInt(); // tarifa

                            //seteo los datos para guardarlos en registros
                            c.setPlaca(placa);
                            c.setPlaza(plaza);
                            Date date = new Date();
                            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
                            DateFormat hourFormat = new SimpleDateFormat("HH:mm:ss");
                            c.setHora_ingreso(hourFormat.format(date));
                            c.setFecha_ingreso(dateFormat.format(date));
                            c.setTarifa(1);
                            c.setEstado(1);
                            c.guardar();

                            //convierto el estado a cero
                            d.setId(plaza);
                            d.setEstado("0");
                            d.actualizar();

                            System.out.println("Registro de vehículo añadido");

                            break;
                        case 3:

                            //fecha y HORA ACTUAL
                            Date date2 = new Date();
                            DateFormat dateFormato = new SimpleDateFormat("yyyy-MM-dd");
                            DateFormat hourFormato = new SimpleDateFormat("HH:mm:ss");
                            String fecha_actual = dateFormato.format(date2);
                            String hora_actual = hourFormato.format(date2);
                            System.out.println(fecha_actual + hora_actual);
                            LocalTime t = LocalTime.parse(hora_actual); //convertir el string a tipo hora 
                            System.out.println(t);
                            Plaza h = new Plaza();
                            Registros g = new Registros(); //instancia
                            Factura f = new Factura(); //instancia
                            System.out.println("Digite la placa");
                            String placaF = s.next(); // pedimos la placa

                            String sentencia = "SELECT fecha_ingreso,hora_ingreso FROM registros WHERE placa = '" + placaF + "' AND estado = 1 ";
                            ResultSet rs = Conexion.ejecutarConsulta(sentencia);//traigo el registro que concuerde con la placa y su estado sea 1 (activo)
                            while (rs.next()) // 
                            {
                                System.out.println("fecha: " + rs.getString(1) + " hora : " + rs.getString(2));
                                LocalTime t2 = LocalTime.parse(rs.getString(2));// convierto el string en hora
                                long elapsedMinutes = Duration.between(t2, t).toMinutes();//encuentro la diferencia en minutos
                                long valor = (elapsedMinutes / 15) * 900; //calculo el valor de la factura con valor de $900 la fracción
                                f.setPlaca(placaF);
                                f.setFecha_ingreso(rs.getString(1));
                                f.setFecha_salida(fecha_actual);
                                f.setHora_ingreso(rs.getString(2));
                                f.setHora_salida(hora_actual);
                                double converted = (double) valor; //convierto el long a double
                                f.setValor(converted);
                                f.setTarifa(1);
                                f.guardar();

                                g.setPlaca(placaF);//actualizo el estado del registro con la placa digitada
                                g.setEstado(0);
                                g.actualizar();

                            }

                            while (rs.next()) // 
                            {
                                System.out.println("fecha: " + rs.getString(1) + " hora : " + rs.getString(2));
                                LocalTime t2 = LocalTime.parse(rs.getString(2));// convierto el string en hora
                                long elapsedMinutes = Duration.between(t2, t).toMinutes();//encuentro la diferencia en minutos
                                long valor = (elapsedMinutes / 15) * 900;
                                f.setPlaca(placaF);
                                f.setFecha_ingreso(rs.getString(1));
                                f.setFecha_salida(fecha_actual);
                                f.setHora_ingreso(rs.getString(2));
                                f.setHora_salida(hora_actual);
                                double converted = (double) valor;
                                f.setValor(converted);
                                f.setTarifa(1);
                                f.guardar();

                                g.setPlaca(placaF);//actualizo el estado del registro con la placa digitada
                                g.setEstado(0);
                                g.actualizar();

                            }
                            h.consultar2();
                            break;
                        case 4:
                            Registros j = new Registros();
                            System.out.println("Digite la placa");
                            String placaC = s.next(); // pedimos la placa
                            j.setPlaca(placaC);
                            j.consultar();
                            break;
                    }

                }

            }

        } else {
            System.out.println("************************************");
            System.out.println("No existe en la base de datos");
            System.out.println("************************************");

        }

    }
}
