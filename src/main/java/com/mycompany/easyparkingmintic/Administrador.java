/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.easyparkingmintic;

import conexion.Conexion;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author JorgeLeoPérez
 */
public class Administrador {

    private int id;
    private String nombre;

    public Administrador(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    Administrador() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    //Consultar
    public boolean consultar() throws ClassNotFoundException, SQLException {
        //CRUD -R 
        Conexion con = new Conexion();
        String sql = "SELECT idadministradores,nombre_admin FROM administradores WHERE idadministradores = '" + this.getId() + "'";
        ResultSet rs = Conexion.ejecutarConsulta(sql);
        while (rs.next()) // 
        {
            this.setId(rs.getInt("idadministradores"));
            this.setNombre(rs.getString("nombre_admin"));

        }

        if (this.getNombre() != "" && this.getNombre() != null) {
            return true;
        } else {
            return false;
        }

    }

    @Override
    public String toString() {
        return "Administrador{" + "id=" + id + ", nombre=" + nombre + '}';
    }

}
