/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.easyparkingmintic;

import conexion.Conexion;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author JorgeLeoPérez
 */
public class Registros {

    private int id;
    private String placa;
    private String fecha_ingreso;
    private String hora_ingreso;
    private int plaza;
    private int tarifa;
    private int estado;

    public Registros(int id, String placa, String fecha_ingreso, String hora_ingreso, int plaza, int tarifa, int estado) {
        this.id = id;
        this.placa = placa;
        this.fecha_ingreso = fecha_ingreso;
        this.hora_ingreso = hora_ingreso;
        this.plaza = plaza;
        this.tarifa = tarifa;
        this.estado = estado;
        
    }

    Registros() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getFecha_ingreso() {
        return fecha_ingreso;
    }

    public void setFecha_ingreso(String fecha_ingreso) {
        this.fecha_ingreso = fecha_ingreso;
    }

    public String getHora_ingreso() {
        return hora_ingreso;
    }

    public void setHora_ingreso(String hora_ingreso) {
        this.hora_ingreso = hora_ingreso;
    }

    public int getPlaza() {
        return plaza;
    }

    public void setPlaza(int plaza) {
        this.plaza = plaza;
    }

    public int getTarifa() {
        return tarifa;
    }

    public void setTarifa(int tarifa) {
        this.tarifa = tarifa;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }
    

    //Guardar
    public void guardar() throws ClassNotFoundException, SQLException {
        //CRUD - C
        Conexion con = new Conexion(); //instancia
        String sql = "INSERT INTO registros(id, placa, fecha_ingreso, hora_ingreso,id_plaza,tarifa,estado) VALUES(" + getId() + ",'" + getPlaca() + "','" + getFecha_ingreso() + "','" + getHora_ingreso() + "','" + getPlaza() + "','" + getTarifa() + "','" + getEstado() + "');";
        ResultSet rs = Conexion.ejecutarConsulta(sql);

    }
    //Consultar

    public boolean consultar() throws ClassNotFoundException, SQLException {
        //CRUD -R 
        Conexion con = new Conexion();
        String sql = "SELECT id,placa,fecha_ingreso,hora_ingreso FROM registros WHERE placa = '" + this.getPlaca() + "'";
        ResultSet rs2 = Conexion.ejecutarConsulta(sql);
        while (rs2.next()) // 
        {
            this.setId(rs2.getInt("id"));
            this.setPlaca(rs2.getString("placa"));
            this.setFecha_ingreso(rs2.getString("fecha_ingreso"));
            this.setHora_ingreso(rs2.getString("hora_ingreso"));
            System.out.println("id: " + rs2.getString(1) + " Placa : " + rs2.getString(2)+ " Fecha Ingreso : " + rs2.getString(3) + " Hora Ingreso : " + rs2.getString(4)); 

        }

        if (this.getId() != 0) {
            return true;
        } else {
            return false;
        }

    }
    //Actualizar

    public void actualizar() throws ClassNotFoundException, SQLException {
        // CRUD -U
        Conexion con = new Conexion();
        //String sql = "UPDATE registros SET fecha_ingreso = '" + getFecha_ingreso() + "', hora_ingreso = '" + getHora_ingreso() + "' plaza = " + getPlaza() + "' tarifa = " + getTarifa() + "' estado = " + getEstado() + " WHERE placa =" + getPlaca() + ";";
        //String sql = "UPDATE registros SET fecha_ingreso = '" + getFecha_ingreso() + "', hora_ingreso = '" + getHora_ingreso() + "' plaza = " + getPlaza() + "' tarifa = " + getTarifa() + "' estado = " + getEstado() + " WHERE placa =" + getPlaca() + ";";
        //String sql = "UPDATE registros SET placa = '" + getPlaca() + "', fecha_ingreso = '" + getFecha_ingreso() + "', hora_ingreso = '" + getHora_ingreso() + "', plaza = '" + getPlaza() + "', tarifa = '" + getTarifa() + "', estado = " + getEstado() + " WHERE placa =" + getPlaca() + ";";
        String sql2 = "UPDATE registros SET estado = " + getEstado() + " WHERE placa ='" + getPlaca() + "';";

        ResultSet rs = Conexion.ejecutarConsulta(sql2);
        System.out.println("Registro actualizado correctamente");

    }
    // Borrar

    public void borrar() throws SQLException, ClassNotFoundException {
        //CRUD -D
        Conexion con = new Conexion();
        String sql = "DELETE FROM registros WHERE id =" + getId() + ";";
        ResultSet rs = Conexion.ejecutarConsulta(sql);
        System.out.println("Registro borrado correctamente");

    }

    @Override
    public String toString() {
        return "Registros{" + "id=" + id + ", placa=" + placa + ", fecha_ingreso=" + fecha_ingreso + ", hora_ingreso=" + hora_ingreso + ", plaza=" + plaza + ", tarifa=" + tarifa + ", estado=" + estado + '}';
    }

   
}
